package gojwt

import (
	"os"

	"github.com/dgrijalva/jwt-go"
)

func ToString(token *jwt.Token) string {
	sToken, err := token.SignedString(os.Getenv("PRIVATE_KEY"))

	if err != nil {
		panic(err)
	}

	return sToken
}
