package gojwt

import (
	"errors"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
)

func ParseFromRequest(req *http.Request) *jwt.Token {

	strToken, err := request.HeaderExtractor{"Authorization"}.ExtractToken(req)
	if err != nil {
		panic(err)
	}
	return Parse(strToken)
}

func Parse(sToken string) *jwt.Token {

	token, err := jwt.Parse(sToken, keyFunction)
	if err != nil {
		switch err.(*jwt.ValidationError).Errors {
		case jwt.ValidationErrorExpired:
			httpAsert.NoError(err, 423)
		default:
			httpAsert.NoError(err, http.StatusBadRequest)
		}
	}

	return token
}

func keyFunction(token *jwt.Token) (interface{}, error) {

	if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
		return nil, errors.New("Unexpected Signing MethodMsg")
	}
	return os.Getenv("PUBLIC_KEY"), nil
}
