package gojwt

import (
	"os"

	"github.com/dgrijalva/jwt-go"
)

func GenerateJWT(claims map[string]interface{}) string {

	token := jwt.New(jwt.SigningMethodHMAC)

	for key, claim := range claims {
		token.Claims.(jwt.MapClaims)[key] = claim
	}

	sToken, err := token.SignedString(os.Getenv("PRIVATE_KEY"))
	if err != nil {
		panic(err)
	}
	return sToken
}
