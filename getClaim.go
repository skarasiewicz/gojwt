package gojwt

import (
	"github.com/dgrijalva/jwt-go"
)

func GetClaim(token *jwt.Token, claim string) string {
	email, ok := token.Claims.(jwt.MapClaims)[claim].(string)
	if !ok {
		panic("Invalid token.")
	}

	return email
}
